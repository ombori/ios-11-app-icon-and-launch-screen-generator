# iOS 11 App Icon and Launch Screen Generator #

A sketch file to help design and export iOS 11 App Icons and Launch Screens at all sizes.


![iOS 11 App Icon and Launch Screen Generator](http://work.rossmalpass.co.uk/ombori/app-icon-generator.png)

## How to use it ##

The generator is split into two pages: **iOS Icons** and **Launch screens**.

### App Icons ###
1. Edit the **iOS App Icon** symbol.
2. See it update live for all sizes as you work.
3. Click the Export button in the menu bar and choose where you want to save everything.

### Launch Screens ###
1. Edit the **iOS Launch Screen** symbol.
2. See it update live for all sizes as you work. A good Launch Screen should closely match the first screen of your app. Use Sketch's resizing options on your elements to make them consistent across all sizes and orientations.
3. Click the Export button in the menu bar and choose where you want to save everything.